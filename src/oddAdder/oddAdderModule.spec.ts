const sumOddNumbers = require("./oddAdderModule.ts");

test("Numbers 1-5", () => {
  expect(sumOddNumbers(1, 2, 3, 4, 5)).toBe(9);
});

test("Only even numbers", () => {
  expect(sumOddNumbers(2, 4, 6)).toBe(0);
});

test("Letters and numbers", () => {
  expect(sumOddNumbers("a", 2, 3, "foo", 4, 5, 6)).toBe(8);
});

test("Only letters", () => {
  expect(sumOddNumbers("a", "b", "c")).toBe(0);
});

test("No input", () => {
  expect(sumOddNumbers()).toBe(0);
});

test("null input", () => {
  expect(sumOddNumbers(null)).toBe(0);
});

test("Zeros", () => {
  expect(sumOddNumbers(0, 0, 0)).toBe(0);
});

test("Only negative numbers", () => {
  expect(sumOddNumbers(-1, -2, -3, -4, -5)).toBe(-9);
});

test("Positive and negative numbers", () => {
  expect(sumOddNumbers(-1, -2, -3, 4, 5, 6, 7)).toBe(8);
});

test("Hex numbers", () => {
  expect(sumOddNumbers(0x01, 0x02, 0x0f)).toBe(16);
});

test("Symbols and other garbage", () => {
  expect(sumOddNumbers("%*!%#$", "jhf*$%#62", 3)).toBe(3);
});

test("Huge numbers/overflow. This should fail because we're not handling it at all", () => {
  expect(
    sumOddNumbers(
      999999999999999999999,
      2222222222222222222,
      111111111111111111111111,
    ),
  ).toBe("for sure not 0");
});

test("Floats. Also not handled and should fail", () => {
  expect(sumOddNumbers(2.5, 1.7, 3.2, 5.4)).toBe(4.2);
});
