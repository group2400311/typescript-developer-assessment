// Take arbitrary arrays and sum up only odd numbers in them
// Treats non-numbers as 0 and doesn't handle floats or int overflow
export function sumOddNumbers(...numbers: number[]) {
  let oddNumbers: number[] = numbers.filter(
    (currentValue: number) => currentValue % 2,
  );
  let sum: number = oddNumbers.reduce(
    (total: number, currentValue: number) => total + currentValue,
    0,
  );
  return sum;
}

module.exports = sumOddNumbers;
