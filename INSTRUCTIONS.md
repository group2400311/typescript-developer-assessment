Hello! I for sure spent more than 2 hours on this but I wasn't timing myself. More time than I'd like to admit went to import/export a function in a way that worked with both Jest and `ts-node`. This was the first JavaScript/TypeScript I've written and my desire to run it from the shell led me to `ts-node`. (Because `node` doesn't seem to like TypeScript)

After cloning this as a fresh repo, I had to run `npm i --save-dev @types/node` to get things working again

### Exercise one

This should be runnable as specified in the README with `npm run cypress:open`

### Exercise two

For the shell, there's a little script in `./` I was running with `ts-node add.ts`

`npm run test` should show 2 failing tests for conditions this simple implementation can't handle
