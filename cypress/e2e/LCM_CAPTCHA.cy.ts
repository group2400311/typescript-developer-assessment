describe("LCM contact form submission without CAPTCHA", () => {
  it('Looks for the CAPTCHA error after clicking "Send"', () => {
    cy.visit("https://lastcallmedia.com/contact");

    cy.get('[id="edit-field-name-0-value"]').type("R. Daneel Olivaw");
    cy.get('[id="edit-mail"]').type("test@email.localdomain");
    cy.get('[id="edit-message-0-value"]').type("Cypress was here");

    // Ignoring this error because:
    // IDK why it's happening and I want to check for the CAPTCHA error message
    cy.on("uncaught:exception", (err, runnable) => {
      expect(err.message).to.include("Cannot read properties of undefined");
      return false;
    });

    cy.contains("Send").click();
    cy.contains("The answer you entered for the CAPTCHA was not correct.");
  });
});
