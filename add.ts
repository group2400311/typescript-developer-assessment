// Intended to be invoked from the command line with ts-node to run the corresponding module

const sumOddNumbers = require("./src/oddAdder/oddAdderModule.ts");

console.log("Adding only odd numbers from 1-7. Should equal 16");
console.log(sumOddNumbers(1, 2, 3, 4, 5, 6, 7));
